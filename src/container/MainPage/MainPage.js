import React, {Component} from 'react';
import Menu from "../../component/Menu/Menu";
import MainPic from "../../component/MainPic/MainPic";
import Footer from "../../component/Footer/Footer";

import "./MainPage.css";



class MainPage extends Component {

    goToAbout = () => {
        this.props.history.push({
            pathname: '/about'
        });
    };

    goToFood = () => {
        this.props.history.push({
            pathname: '/food'
        });
    };

    goToContacts = () => {
        this.props.history.push({
            pathname: '/contacts'
        });
    };

    render() {
        return (
            <div className="Body">
                <Menu
                    onAbout={this.goToAbout}
                    onFood={this.goToFood}
                    onContacts={this.goToContacts}/>
                <MainPic/>
                <Footer/>
            </div>
        );
    }
}

export default MainPage;