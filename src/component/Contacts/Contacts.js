import React, {Component, Fragment} from 'react';
import './Contacts.css';
import Modal from "../UI/Modal/Modal";
import Footer from "../Footer/Footer";

class Contacts extends Component {
    state = {
        submit: false,
        modal: [{name: 'Your name', phone: +996, email: 'example@com'}]

    };

    modalCancel = () => {
        this.setState({submit: false})
    };

    changeValue = (event) => {
        let modal = [...this.state.modal];
        let obj = {...this.state.modal[0]};
        let name = event.target.name;
        obj[name] = event.target.value;
        modal[0] = obj;
        this.setState({modal})
    };
    changeFocus = (event) => {
        let modal = [...this.state.modal];
        let obj = {...this.state.modal[0]};
        let name = event.target.name;
        obj[name] = ' ';
        modal[0] = obj;
        this.setState({modal})
    };
    submit = () => {
        this.setState({submit: true})
    };

    render() {
        return (
            <Fragment>
            <div className="Contacts">
                <img src="https://www.bbcgoodfood.com/sites/default/files/guide/guide-image/2017/01/health-fitness-instagram-to-follow-main-image-700-350.jpg" alt="contacts"/>
                <h2 className="ContactsTitle">Let's connect!</h2>
                <p>Partners|Sponsors|Collaborations</p>
                <p className="Mail">aidzhenalieva@gmail.com</p>
                <p>General inquiries</p>
                <p className="Mail">info@healthyfooddaily.com</p>

                <button className="Submit" onClick={this.submit}>Submit your application for healthy diet</button>
                {this.state.submit ?
                    <Modal show={this.state.submit}
                           close={this.modalCancel}
                           name={this.state.modal[0].name}
                           phone={this.state.modal[0].phone}
                           email={this.state.modal[0].email}
                           onFocus={event => this.changeFocus(event)}
                           changeValue={event => this.changeValue(event)}

                    /> : null
                }
            </div>
                <Footer />
            </Fragment>
        );
    }
}

export default Contacts;