import React from 'react';
import './Food.css';

const Food = () => {
    return (
        <div className="Food">
            <h2>Happy recipes</h2>
            <div>
                <h4>One-Pot Lasagna Soup</h4>
                <img
                    src="https://static.wixstatic.com/media/26357d_20d084319d6c477b83e811a1cad51eca~mv2_d_5184_3888_s_4_2.jpg/v1/fill/w_630,h_473,al_c,q_80,usm_0.66_1.00_0.01/26357d_20d084319d6c477b83e811a1cad51eca~mv2_d_5184_3888_s_4_2.webp"
                    alt="food"/>
                <p>Lasagna is one of those foods that I just crave from time to time, with its warm and inviting
                    layers,
                    and that incredible aroma that fills your kitchen when you prepare it. But assembling and baking
                    a
                    lasagna is much more time consuming than any of us care for. Enter lasagna soup! All the
                    heartiness
                    of the real deal, but made with the ease of a one-pot dish.
                    At first I thought this soup would need a creamy topping, like a nut cheese, but I'm happy to
                    report
                    that it doesn't need it at all! It's delicious to enjoy all on its own! </p>
                <p>
                    Yield: 4-5 servings <br/>
                    Prep Time: 10 min <br/>
                    Cook Time: 20-25 min <br/>
                    Total Time: 30-35 min <br/>

                    INGREDIENTS <br/>

                    2 tsp (10 ml) olive oil <br/>
                    1 yellow onion, diced <br/>
                    3 cloves garlic, minced <br/>
                    2 Tbsp (30 ml) tomato paste <br/>
                    2 vegetable boullion cubes <br/>
                    1 tsp (5 g) Italian seasoning <br/>
                    1 tsp (5 g) dried oregano <br/>
                    1 tsp (5 g) onion powder <br/>
                    1/2 tsp dried thyme leaves <br/>
                    1/4 tsp salt <br/>
                    1/4 tsp black pepper <br/>


                    1 package (200 g) veggie ground beef* <br/>
                    2 cups (250 g) cremini mushrooms, diced <br/>
                    2 cans (800 g) diced tomatoes, with juice <br/>
                    2 tsp (10 ml) balsamic vinegar <br/>

                    8-10 uncooked lasagna noodles, broken into bite sized pieces <br/>
                    2 cups (100 g) fresh spinach <br/>
                </p>

                <p>Directions <br/>
                    Heat the oil in a large pot over medium-high heat. Add the onion and sauté until lightly golden,
                    about 5 minutes, adding a splash of water as needed to deglaze the pan. Add garlic, tomato
                    paste,
                    bullion cubes and spices and cook for an additional 1-2 minutes.
                    Add the mushrooms and ground vegan meat, cooking until much of the moisture from the mushrooms
                    evaporates, about 5 minutes.

                    Add the diced tomatoes, and 2 cans of water (rinsing out each of the tomato cans), and balsamic
                    vinegar. Bring to a boil then add the noodles. Cook for 8-10 minutes or until the noodles are
                    tender. Stir in the spinach at the very end and remove from heat. Top with fresh basil and
                    enjoy!

                    Notes <br/>

                    *Variations: substitute the veggie grounds for 3/4 cup cooked brown lentils. Try adding carrots,
                    red
                    peppers, green beans, or peas.
                    Storage: store in an air-tight container in the fridge for up to 4 days.</p>
            </div>
            <div>
                <h4>Creamy Coconut Paprika Zucchini Soup</h4>
                <img
                    src="https://static.wixstatic.com/media/26357d_ba9284fcb1bc42058cf9dc594f7d5d9e~mv2_d_6000_4000_s_4_2.jpg/v1/fill/w_630,h_420,al_c,q_80,usm_0.66_1.00_0.01/26357d_ba9284fcb1bc42058cf9dc594f7d5d9e~mv2_d_6000_4000_s_4_2.webp"
                    alt="food"/>
                <p>This soup is luscious; fantastic for the fall, with bold, warm, and rich flavours. So yummy and
                    quick, that we've made it twice in the last week, and we made a double batch for a huge party
                    here
                    at the studio last weekend. It was a hit - all finished in under 5 minutes! </p>
                <p>
                    Yield: 4-5 servings <br/>
                    Prep Time: 10 min <br/>
                    Cook Time: 20-25 min <br/>
                    Total Time: 30-35 min <br/>

                    INGREDIENTS <br/>

                    1 tsp (5 ml) sunflower oil <br/>
                    2 yellow onions, roughly diced <br/>
                    2 cloves garlic, crushed <br/>
                    2 cups (500 ml) water <br/>
                    2 vegetable boullion cubes <br/>
                    1 zucchini, roughly chopped <br/>
                    3 bell peppers, roughly chopped <br/>
                    1 tsp (5 g) ground coriander <br/>
                    1/4 cup (62 g) sun-dried bell pepper (aka paprika) in oil <br/>
                    1 can (400 ml) full-fat coconut milk <br/>


                </p>

                <p>Directions <br/>
                    Heat the oil in a large soup pot over medium-high heat. Add onions and sauté until lightly brown
                    and soft, about 5 minutes. Add splashes of water as needed to decaramelize the pan and prevent
                    burning and sticking. Add crushed garlic and bouillon cubes and stir until the cubes have
                    dissolved
                    and the garlic is fragrant - about 1-2 minutes.
                    Add zucchini, bell peppers, coriander, and sun-dried paprika to the pot and sauté for 4-5
                    minutes.
                    Add the remaining water.
                    Bring everything to a boil, then reduce heat to low, and simmer with lid partially covered for
                    10-15 minutes.
                    Add the coconut milk and cook for another 3-5 minutes, then blend the soup with an immersion
                    blender until creamy. Garnish and enjoy! <br/>

                    Notes <br/>

                    Garnishing: top with crunchy croutons, rustic bread, fried onions, black sesame seeds, roasted
                    chickpeas, fresh thyme or basil, or some creamy coconut milk. <br/>
                    Variations: if desired, you can use roasted red peppers in water instead of sun-dried paprika in
                    oil, low-fat coconut milk, or cumin instead of coriander powder. <br/>
                    Leftovers: this is a great batch-cooking recipe. Prepare more than you need and freeze some for
                    later. This recipe also works great for whenever you find yourself with vegetables that are
                    nearing
                    the end of their shelf life. <br/>
                    Storage: store in an air-tight container in the fridge for up to 4 days or in the freezer for up
                    to
                    2 months. .</p>
            </div>

            <a className="FoodSeeMore" href="https://www.pickuplimes.com/recipe-index">See more recipes</a>
            <p>*all recipes were taken from "PickUpLimes blog
                "</p>
        </div>
    )
};

export default Food;