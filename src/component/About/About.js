import React, {Fragment} from 'react';
import './About.css';
import Footer from "../Footer/Footer";

const About = () => {
    return (
        <Fragment>
        <div className="About">
            <img className="AboutImg"
                 src="http://www.beattractive.in/wp-content/uploads/2018/05/03-consuming-healthy-food-habits-you-should-drop-417496159-colnihko-1024x683.jpg"
                 alt=""/>
            <h2>Why "Healthy Food Daily"?</h2>
            <p>Importance of Good Nutrition
                Your food choices each day affect your health — how you feel today, tomorrow, and in the future.
                Good nutrition is an important part of leading a healthy lifestyle. Combined with physical activity,
                your diet can help you to reach and maintain a healthy weight, reduce your risk of chronic diseases
                (like heart disease and cancer), and promote your overall health.</p>
            <p>The Impact of Nutrition on Your Health
                Unhealthy eating habits have contributed to the obesity epidemic in the United States: about one-third
                of U.S. adults (33.8%) are obese and approximately 17% (or 12.5 million) of children and adolescents
                aged 2—19 years are obese.1 Even for people at a healthy weight, a poor diet is associated with major
                health risks that can cause illness and even death. These include heart disease, hypertension (high
                blood pressure), type 2 diabetes, osteoporosis, and certain types of cancer. By making smart food
                choices, you can help protect yourself from these health problems.</p>

            <p> The risk factors for adult chronic diseases, like hypertension and type 2 diabetes, are increasingly
                seen in younger ages, often a result of unhealthy eating habits and increased weight gain. Dietary
                habits established in childhood often carry into adulthood, so teaching children how to eat healthy at a
                young age will help them stay healthy throughout their life.</p>

            <p>The link between good nutrition and healthy weight, reduced chronic disease risk, and overall health is
                too important to ignore. By taking steps to eat healthy, you'll be on your way to getting the nutrients
                your body needs to stay healthy, active, and strong. As with physical activity, making small changes in
                your diet can go a long way, and it's easier than you think!</p>
            <img className="AboutImg2" src="http://aura.ba//wp-content/uploads/2017/06/ishrana-po-duginim-bojama.jpg" alt="secondPic"/>
            <p>The Impact of Nutrition on Your Health
                Unhealthy eating habits have contributed to the obesity epidemic in the United States: about one-third
                of U.S. adults (33.8%) are obese and approximately 17% (or 12.5 million) of children and adolescents
                aged 2—19 years are obese.1 Even for people at a healthy weight, a poor diet is associated with major
                health risks that can cause illness and even death. These include heart disease, hypertension (high
                blood pressure), type 2 diabetes, osteoporosis, and certain types of cancer. By making smart food
                choices, you can help protect yourself from these health problems.</p>

            <p> The risk factors for adult chronic diseases, like hypertension and type 2 diabetes, are increasingly
                seen in younger ages, often a result of unhealthy eating habits and increased weight gain. Dietary
                habits established in childhood often carry into adulthood, so teaching children how to eat healthy at a
                young age will help them stay healthy throughout their life.</p>

            <p>The link between good nutrition and healthy weight, reduced chronic disease risk, and overall health is
                too important to ignore. By taking steps to eat healthy, you'll be on your way to getting the nutrients
                your body needs to stay healthy, active, and strong. As with physical activity, making small changes in
                your diet can go a long way, and it's easier than you think!</p>

        </div>
            <Footer/>
        </Fragment>
    );
};

export default About;