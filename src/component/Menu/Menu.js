import React from 'react';
import './Menu.css';

const Menu = props => {
    return (
        <div>
            <nav className="Nav">
                <p className="Logo">Healthy Food Daily</p>
                <ul className='Menu'>
                    <li><button onClick={props.onAbout}>About</button></li>
                    <li><button onClick={props.onFood}>Food</button></li>
                    <li><button onClick={props.onContacts}>Contacts</button></li>

                </ul>
            </nav>
        </div>
    );
};

export default Menu;