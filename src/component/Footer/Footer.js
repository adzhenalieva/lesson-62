import React from 'react';
import './Footer.css';

const Footer = () => {
    return (
        <div className="Footer">
            <p>Copyright @ 2019. All rights reserved</p>
        </div>
    );
};

export default Footer;