import React from 'react';
import './MainPic.css';

const MainPic = () => {
    return (
        <div className="MainPic">
            <h1>To happy life through healthy food</h1>
            <p>Eco, healthy, natural</p>
        </div>
    );
};

export default MainPic;