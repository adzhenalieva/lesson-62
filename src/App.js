import React, { Component } from 'react';
import MainPage from "./container/MainPage/MainPage";
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import About from "./component/About/About";
import Food from "./component/Food/Food";
import Contacts from "./component/Contacts/Contacts";

import './App.css';



class App extends Component {
  render() {
    return (
      <div className="App">
          <BrowserRouter>
              <Switch>
                  <Route path="/about" component={About}/>
                  <Route path="/food" component={Food}/>
                  <Route path="/contacts" component={Contacts}/>
                  <Route path="/" exact component={MainPage} />
              </Switch>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
